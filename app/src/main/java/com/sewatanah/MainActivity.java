package com.sewatanah;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toolbar;

import com.sewatanah.Utils.SharedPrefManager;
import com.sewatanah.fragment.fragmentAkun;
import com.sewatanah.fragment.fragmentDashboard;
import com.sewatanah.fragment.getSession.fragmentLogin;
import com.sewatanah.fragment.getSession.fragmentRegister;

public class MainActivity extends AppCompatActivity {

    android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
    SharedPrefManager spManager;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragmentManager.beginTransaction()
                            .replace(R.id.frameLayout, new fragmentDashboard())
                            .commit();
                    getSupportActionBar().setTitle("Dashboard");
                    return true;
                case R.id.navigation_dashboard:
                    fragmentManager.beginTransaction()
                            .replace(R.id.frameLayout, new fragmentRegister())
                            .commit();
                    getSupportActionBar().setTitle("Cari Lahan");
                    return true;
                case R.id.navigation_notifications:
                    if (!spManager.getSPSudahLogin()){
                        fragmentManager.beginTransaction()
                                .replace(R.id.frameLayout, new fragmentLogin())
                                .commit();
                        getSupportActionBar().setTitle("Masuk Akun");
                    }else{
                        fragmentManager.beginTransaction()
                                .replace(R.id.frameLayout, new fragmentAkun())
                                .commit();
                        getSupportActionBar().setTitle("Akun Anda");
                    }
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spManager = new SharedPrefManager(this);

        fragmentManager.beginTransaction()
                .replace(R.id.frameLayout, new fragmentDashboard())
                .commit();
        getSupportActionBar().setTitle("Dashboard");

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
