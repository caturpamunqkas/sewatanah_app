package com.sewatanah.Adapter.LahanAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sewatanah.Models.GetLahan.ResultsItem;
import com.sewatanah.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterLahan extends RecyclerView.Adapter<AdapterLahan.MyViewHolder> {
    private List<ResultsItem> resultsItems;

    public AdapterLahan(List<ResultsItem> resultsItemList) {
        resultsItems = resultsItemList;
    }

    @NonNull
    @Override
    public AdapterLahan.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cardview_dashboard, parent, false);
        return new MyViewHolder(mView);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder,final int position) {
        Picasso.with(holder.itemView.getContext())
                .load(resultsItems.get(position).getUrlimage())
                .into(holder.imgLahan);
        holder.judulLahan.setText(resultsItems.get(position).getJudul());

    }

    @Override
    public int getItemCount() {
        return resultsItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgLahan;
        TextView judulLahan;

        MyViewHolder(View itemView) {
            super(itemView);

            imgLahan = itemView.findViewById(R.id.imgLahan);
            judulLahan = itemView.findViewById(R.id.judulLahan);
        }
    }
}
