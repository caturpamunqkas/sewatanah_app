package com.sewatanah.Adapter.LahanAdapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sewatanah.Models.GetLahan.ResultsItem;
import com.sewatanah.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterLahan2 extends RecyclerView.Adapter<AdapterLahan2.MyViewHolder> {
    private List<ResultsItem> resultsItems;

    public AdapterLahan2(List<ResultsItem> resultsItemList) {
        resultsItems = resultsItemList;
    }

    @NonNull
    @Override
    public AdapterLahan2.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cardview_2_dashboard, parent, false);
        return new MyViewHolder(mView);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder,final int position) {
        String BASE_URL = "http://192.168.43.170/sewatanah_web/";
        Picasso.with(holder.itemView.getContext())
                .load(BASE_URL +"assets/tanah_picture/"+resultsItems.get(position).getFotoLahan())
                .into(holder.imgLahan);
        holder.judulLahan.setText(resultsItems.get(position).getJudul());
    }

    @Override
    public int getItemCount() {
        return resultsItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgLahan;
        TextView judulLahan;

        MyViewHolder(View itemView) {
            super(itemView);

            imgLahan = itemView.findViewById(R.id.imgLahan);
            judulLahan = itemView.findViewById(R.id.judulLahan);
        }
    }
}
