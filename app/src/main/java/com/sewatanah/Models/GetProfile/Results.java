package com.sewatanah.Models.GetProfile;

import com.google.gson.annotations.SerializedName;
import com.sewatanah.Models.GetUser.User;

public class Results{

	@SerializedName("user")
	private com.sewatanah.Models.GetUser.User user;

	@SerializedName("lahan")
	private Lahan lahan;

	public void setUser(com.sewatanah.Models.GetUser.User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public void setLahan(Lahan lahan){
		this.lahan = lahan;
	}

	public Lahan getLahan(){
		return lahan;
	}

	@Override
 	public String toString(){
		return 
			"Results{" + 
			"user = '" + user + '\'' + 
			",lahan = '" + lahan + '\'' + 
			"}";
		}
}