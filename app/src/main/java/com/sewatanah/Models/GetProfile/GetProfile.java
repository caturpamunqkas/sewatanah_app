package com.sewatanah.Models.GetProfile;

import com.google.gson.annotations.SerializedName;

public class GetProfile{

	@SerializedName("success")
	private Success success;

	@SerializedName("results")
	private Results results;

	@SerializedName("status")
	private boolean status;

	public void setSuccess(Success success){
		this.success = success;
	}

	public Success getSuccess(){
		return success;
	}

	public void setResults(Results results){
		this.results = results;
	}

	public Results getResults(){
		return results;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetProfile{" + 
			"success = '" + success + '\'' + 
			",results = '" + results + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}