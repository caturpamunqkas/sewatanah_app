package com.sewatanah.Models.GetProfile;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Lahan{

	@SerializedName("total")
	private int total;

	@SerializedName("notverif")
	private int notverif;

	@SerializedName("terverif")
	private int terverif;

	@SerializedName("list")
	private List<ListItem> list;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setNotverif(int notverif){
		this.notverif = notverif;
	}

	public int getNotverif(){
		return notverif;
	}

	public void setTerverif(int terverif){
		this.terverif = terverif;
	}

	public int getTerverif(){
		return terverif;
	}

	public void setList(List<ListItem> list){
		this.list = list;
	}

	public List<ListItem> getList(){
		return list;
	}

	@Override
 	public String toString(){
		return 
			"Lahan{" + 
			"total = '" + total + '\'' + 
			",notverif = '" + notverif + '\'' + 
			",terverif = '" + terverif + '\'' + 
			",list = '" + list + '\'' + 
			"}";
		}
}