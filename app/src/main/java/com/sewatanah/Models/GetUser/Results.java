package com.sewatanah.Models.GetUser;

import com.google.gson.annotations.SerializedName;

public class Results{

	@SerializedName("nama_depan")
	private String namaDepan;

	@SerializedName("gender")
	private String gender;

	@SerializedName("telepon")
	private String telepon;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id_user")
	private int idUser;

	@SerializedName("profesi")
	private String profesi;

	@SerializedName("nama_belakang")
	private String namaBelakang;

	@SerializedName("verif")
	private String verif;

	@SerializedName("url")
	private String url;

	@SerializedName("urldelete")
	private String urldelete;

	@SerializedName("alamat")
	private String alamat;

	@SerializedName("imagedir")
	private String imagedir;

	@SerializedName("password")
	private String password;

	@SerializedName("foto")
	private String foto;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("alert")
	private int alert;

	@SerializedName("urlupdate")
	private String urlupdate;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("status")
	private String status;

	public void setNamaDepan(String namaDepan){
		this.namaDepan = namaDepan;
	}

	public String getNamaDepan(){
		return namaDepan;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setTelepon(String telepon){
		this.telepon = telepon;
	}

	public String getTelepon(){
		return telepon;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setIdUser(int idUser){
		this.idUser = idUser;
	}

	public Integer getIdUser(){
		return idUser;
	}

	public void setProfesi(String profesi){
		this.profesi = profesi;
	}

	public String getProfesi(){
		return profesi;
	}

	public void setNamaBelakang(String namaBelakang){
		this.namaBelakang = namaBelakang;
	}

	public String getNamaBelakang(){
		return namaBelakang;
	}

	public void setVerif(String verif){
		this.verif = verif;
	}

	public String getVerif(){
		return verif;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setUrldelete(String urldelete){
		this.urldelete = urldelete;
	}

	public String getUrldelete(){
		return urldelete;
	}

	public void setAlamat(String alamat){
		this.alamat = alamat;
	}

	public String getAlamat(){
		return alamat;
	}

	public void setImagedir(String imagedir){
		this.imagedir = imagedir;
	}

	public String getImagedir(){
		return imagedir;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setFoto(String foto){
		this.foto = foto;
	}

	public String getFoto(){
		return foto;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setAlert(int alert){
		this.alert = alert;
	}

	public int getAlert(){
		return alert;
	}

	public void setUrlupdate(String urlupdate){
		this.urlupdate = urlupdate;
	}

	public String getUrlupdate(){
		return urlupdate;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Results{" + 
			"nama_depan = '" + namaDepan + '\'' + 
			",gender = '" + gender + '\'' + 
			",telepon = '" + telepon + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id_user = '" + idUser + '\'' + 
			",profesi = '" + profesi + '\'' + 
			",nama_belakang = '" + namaBelakang + '\'' + 
			",verif = '" + verif + '\'' + 
			",url = '" + url + '\'' + 
			",urldelete = '" + urldelete + '\'' + 
			",alamat = '" + alamat + '\'' + 
			",imagedir = '" + imagedir + '\'' + 
			",password = '" + password + '\'' + 
			",foto = '" + foto + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",alert = '" + alert + '\'' + 
			",urlupdate = '" + urlupdate + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}