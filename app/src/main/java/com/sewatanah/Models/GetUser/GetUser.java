package com.sewatanah.Models.GetUser;

import com.google.gson.annotations.SerializedName;

public class GetUser {

	@SerializedName("success")
	private Success success;

	@SerializedName("results")
	private User results;

	@SerializedName("status")
	private boolean status;

	public void setSuccess(Success success){
		this.success = success;
	}

	public Success getSuccess(){
		return success;
	}

	public void setResults(User results){
		this.results = results;
	}

	public User getResults(){
		return results;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetUser{" +
			"success = '" + success + '\'' + 
			",results = '" + results + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}