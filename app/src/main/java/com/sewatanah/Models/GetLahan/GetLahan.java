package com.sewatanah.Models.GetLahan;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetLahan{

	@SerializedName("success")
	private Success success;

	@SerializedName("results")
	private List<ResultsItem> results;

	@SerializedName("status")
	private boolean status;

	public void setSuccess(Success success){
		this.success = success;
	}

	public Success getSuccess(){
		return success;
	}

	public void setResults(List<ResultsItem> results){
		this.results = results;
	}

	public List<ResultsItem> getResults(){
		return results;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetLahan{" + 
			"success = '" + success + '\'' + 
			",results = '" + results + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}