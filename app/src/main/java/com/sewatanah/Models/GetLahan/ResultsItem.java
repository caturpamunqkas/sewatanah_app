package com.sewatanah.Models.GetLahan;

import com.google.gson.annotations.SerializedName;

public class ResultsItem{

	@SerializedName("foto_lahan")
	private String fotoLahan;

	@SerializedName("kondisi")
	private String kondisi;

	@SerializedName("luas")
	private String luas;

	@SerializedName("created_at")
	private Object createdAt;

	@SerializedName("urldelete")
	private String urldelete;

	@SerializedName("imagedir")
	private String imagedir;

	@SerializedName("harga")
	private int harga;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("urlupdate")
	private String urlupdate;

	@SerializedName("fasilitas_tanah")
	private String fasilitasTanah;

	@SerializedName("id_lahan")
	private int idLahan;

	@SerializedName("urlstatus")
	private String urlstatus;

	@SerializedName("id")
	private int id;

	@SerializedName("judul")
	private String judul;

	@SerializedName("id_kategori")
	private int idKategori;

	@SerializedName("kurun_sewa")
	private String kurunSewa;

	@SerializedName("alamat_lahan")
	private String alamatLahan;

	@SerializedName("sertifikasi")
	private String sertifikasi;

	@SerializedName("id_user")
	private int idUser;

	@SerializedName("url")
	private String url;

	@SerializedName("fasilitas_jalan")
	private String fasilitasJalan;

	@SerializedName("fasilitas_irigasi")
	private String fasilitasIrigasi;

	@SerializedName("fasilitas_pemandangan")
	private String fasilitasPemandangan;

	@SerializedName("urlimage")
	private String urlimage;

	@SerializedName("deskripsi")
	private String deskripsi;

	@SerializedName("status")
	private String status;

	public void setFotoLahan(String fotoLahan){
		this.fotoLahan = fotoLahan;
	}

	public String getFotoLahan(){
		return fotoLahan;
	}

	public void setKondisi(String kondisi){
		this.kondisi = kondisi;
	}

	public String getKondisi(){
		return kondisi;
	}

	public void setLuas(String luas){
		this.luas = luas;
	}

	public String getLuas(){
		return luas;
	}

	public void setCreatedAt(Object createdAt){
		this.createdAt = createdAt;
	}

	public Object getCreatedAt(){
		return createdAt;
	}

	public void setUrldelete(String urldelete){
		this.urldelete = urldelete;
	}

	public String getUrldelete(){
		return urldelete;
	}

	public void setImagedir(String imagedir){
		this.imagedir = imagedir;
	}

	public String getImagedir(){
		return imagedir;
	}

	public void setHarga(int harga){
		this.harga = harga;
	}

	public int getHarga(){
		return harga;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUrlupdate(String urlupdate){
		this.urlupdate = urlupdate;
	}

	public String getUrlupdate(){
		return urlupdate;
	}

	public void setFasilitasTanah(String fasilitasTanah){
		this.fasilitasTanah = fasilitasTanah;
	}

	public String getFasilitasTanah(){
		return fasilitasTanah;
	}

	public void setIdLahan(int idLahan){
		this.idLahan = idLahan;
	}

	public int getIdLahan(){
		return idLahan;
	}

	public void setUrlstatus(String urlstatus){
		this.urlstatus = urlstatus;
	}

	public String getUrlstatus(){
		return urlstatus;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setJudul(String judul){
		this.judul = judul;
	}

	public String getJudul(){
		return judul;
	}

	public void setIdKategori(int idKategori){
		this.idKategori = idKategori;
	}

	public int getIdKategori(){
		return idKategori;
	}

	public void setKurunSewa(String kurunSewa){
		this.kurunSewa = kurunSewa;
	}

	public String getKurunSewa(){
		return kurunSewa;
	}

	public void setAlamatLahan(String alamatLahan){
		this.alamatLahan = alamatLahan;
	}

	public String getAlamatLahan(){
		return alamatLahan;
	}

	public void setSertifikasi(String sertifikasi){
		this.sertifikasi = sertifikasi;
	}

	public String getSertifikasi(){
		return sertifikasi;
	}

	public void setIdUser(int idUser){
		this.idUser = idUser;
	}

	public int getIdUser(){
		return idUser;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setFasilitasJalan(String fasilitasJalan){
		this.fasilitasJalan = fasilitasJalan;
	}

	public String getFasilitasJalan(){
		return fasilitasJalan;
	}

	public void setFasilitasIrigasi(String fasilitasIrigasi){
		this.fasilitasIrigasi = fasilitasIrigasi;
	}

	public String getFasilitasIrigasi(){
		return fasilitasIrigasi;
	}

	public void setFasilitasPemandangan(String fasilitasPemandangan){
		this.fasilitasPemandangan = fasilitasPemandangan;
	}

	public String getFasilitasPemandangan(){
		return fasilitasPemandangan;
	}

	public void setUrlimage(String urlimage){
		this.urlimage = urlimage;
	}

	public String getUrlimage(){
		return urlimage;
	}

	public void setDeskripsi(String deskripsi){
		this.deskripsi = deskripsi;
	}

	public String getDeskripsi(){
		return deskripsi;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResultsItem{" + 
			"foto_lahan = '" + fotoLahan + '\'' + 
			",kondisi = '" + kondisi + '\'' + 
			",luas = '" + luas + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",urldelete = '" + urldelete + '\'' + 
			",imagedir = '" + imagedir + '\'' + 
			",harga = '" + harga + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",urlupdate = '" + urlupdate + '\'' + 
			",fasilitas_tanah = '" + fasilitasTanah + '\'' + 
			",id_lahan = '" + idLahan + '\'' + 
			",urlstatus = '" + urlstatus + '\'' + 
			",id = '" + id + '\'' + 
			",judul = '" + judul + '\'' + 
			",id_kategori = '" + idKategori + '\'' + 
			",kurun_sewa = '" + kurunSewa + '\'' + 
			",alamat_lahan = '" + alamatLahan + '\'' + 
			",sertifikasi = '" + sertifikasi + '\'' + 
			",id_user = '" + idUser + '\'' + 
			",url = '" + url + '\'' + 
			",fasilitas_jalan = '" + fasilitasJalan + '\'' + 
			",fasilitas_irigasi = '" + fasilitasIrigasi + '\'' + 
			",fasilitas_pemandangan = '" + fasilitasPemandangan + '\'' + 
			",urlimage = '" + urlimage + '\'' + 
			",deskripsi = '" + deskripsi + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}