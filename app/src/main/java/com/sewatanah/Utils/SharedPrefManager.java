package com.sewatanah.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.google.gson.Gson;
import com.sewatanah.Models.GetUser.Results;
import com.sewatanah.Models.GetUser.User;

import java.io.UnsupportedEncodingException;

/**
 * Created by DESKTOP-TJTR on 1/31/2018.
 */

public class SharedPrefManager {
    private static final String SP_SEWATANAH = "spSewaTanah";

    public static final String SP_SUDAH_LOGIN = "spSudahLogin";
    public static final String SP_USER = "userInfo";

    private SharedPreferences sp;
    private SharedPreferences.Editor spEditor;

    @SuppressLint("CommitPrefEdits")
    public SharedPrefManager(Context context){
        sp = context.getSharedPreferences(SP_SEWATANAH, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void saveSPString(String keySP, String value){
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPInt(String keySP, int value){
        spEditor.putInt(keySP, value);
        spEditor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public void saveUser(User user){
        Gson gson = new Gson();
        String json = gson.toJson(user);
        try {
            byte[] data = json.getBytes("UTF-8");
            String enc = Base64.encodeToString(data, Base64.DEFAULT);
            spEditor.putString(SP_USER, enc);
            spEditor.commit();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public User getUser(){
        Gson gson = new Gson();
        String json = sp.getString(SP_USER, "");
        byte[] data = Base64.decode(json, Base64.DEFAULT);
        try {
            String text = new String(data, "UTF-8");
            return gson.fromJson(text, User.class);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean getSPSudahLogin(){
        return sp.getBoolean(SP_SUDAH_LOGIN, false);
    }
}
