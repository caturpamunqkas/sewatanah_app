package com.sewatanah.fragment.getSession;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sewatanah.API.BaseApiService;
import com.sewatanah.API.UtilsApi;
import com.sewatanah.Models.GetUser.GetUser;
import com.sewatanah.R;
import com.sewatanah.Utils.SharedPrefManager;
import com.sewatanah.fragment.fragmentAkun;
import com.sewatanah.fragment.fragmentDashboard;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentLogin extends Fragment {

    TextView txtDaftar;
    EditText txtUser, txtPass;
    BaseApiService mApiService;
    Button btnLogin;
    SharedPrefManager sharedPrefManager;

    public fragmentLogin() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        txtDaftar = view.findViewById(R.id.txtDaftar);
        txtUser = view.findViewById(R.id.inputEmail);
        txtPass = view.findViewById(R.id.inputPassword);
        btnLogin = view.findViewById(R.id.btnLogin);

        sharedPrefManager = new SharedPrefManager(getActivity().getApplicationContext());

        mApiService = UtilsApi.getAPIService();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLogin();
            }
        });

        txtDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, new fragmentRegister(), "Daftar Akun");
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        return view;
    }

    private void requestLogin(){
        mApiService.loginRequest(txtUser.getText().toString(), txtPass.getText().toString())
                .enqueue(new Callback<GetUser>() {
                    @Override
                    public void onResponse(@NonNull Call<GetUser> call, @NonNull Response<GetUser> response) {
                        if (response.isSuccessful()){
                            if (response.isSuccessful()){

                                sharedPrefManager.saveUser(response.body().getResults());
                                sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);

                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frameLayout, new fragmentAkun(), "Akun Anda");
                                ft.addToBackStack(null);
                                ft.commit();

                            } else {
//                                    Toast.makeText(getContext(), jsonObject.get("message").toString(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Pastikan Anda Terhubung Dengan Internet.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetUser> call, @NonNull Throwable t) {

                    }
                });
    }

}
