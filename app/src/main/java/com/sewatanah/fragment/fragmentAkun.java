package com.sewatanah.fragment;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sewatanah.API.BaseApiService;
import com.sewatanah.API.UtilsApi;
import com.sewatanah.Models.GetProfile.GetProfile;
import com.sewatanah.R;
import com.sewatanah.Utils.SharedPrefManager;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentAkun extends Fragment {

    BaseApiService baseApiService;
    SwipeRefreshLayout swLayout;

    SharedPrefManager sharedPrefManager;
    TextView nama, telepon, email, gender, alamat, alertInfo, closer, txtLahan;
    ImageView foto;
    LinearLayout containerAlert;

    public fragmentAkun() {
        // Required empty public constructor
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_akun, container, false);

        nama = view.findViewById(R.id.nama);
        telepon = view.findViewById(R.id.telepon);
        email = view.findViewById(R.id.email);
        gender = view.findViewById(R.id.gender);
        alamat = view.findViewById(R.id.alamat);
        foto = view.findViewById(R.id.fotoUser);
        containerAlert = view.findViewById(R.id.containerAlert);
        alertInfo = view.findViewById(R.id.infoAlert);
        closer = view.findViewById(R.id.close);
        swLayout = view.findViewById(R.id.swlayout);
        txtLahan = view.findViewById(R.id.txtLahan);

        sharedPrefManager = new SharedPrefManager(getContext());
        baseApiService = UtilsApi.getAPIService();

        getProfile();
        getSpProfile();

        swLayout.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swLayout.setRefreshing(false);
                        getProfile();
                    }
                },3000);
            }
        });

        return view;
    }

    @SuppressLint("SetTextI18n")
    public void getSpProfile(){
        nama.setText(sharedPrefManager.getUser().getNamaDepan()+" "+sharedPrefManager.getUser().getNamaBelakang());
        telepon.setText(sharedPrefManager.getUser().getTelepon());
        email.setText(sharedPrefManager.getUser().getEmail());

        if (sharedPrefManager.getUser().getGender().equals("L")){
            gender.setText("Laki - Laki");
        }else{
            gender.setText("Perempuan");
        }

        alamat.setText(sharedPrefManager.getUser().getAlamat());
        Picasso.with(getContext())
                .load(sharedPrefManager.getUser().getImagedir())
                .into(foto);

        closer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                containerAlert.setVisibility(View.GONE);
            }
        });

        if (sharedPrefManager.getUser().getAlert() >= 1){
            containerAlert.setVisibility(View.VISIBLE);
            alertInfo.setText("Anda Mendapatkan "+sharedPrefManager.getUser().getAlert()+" Peringatan.");
        }

        if (sharedPrefManager.getUser().getAlert() >= 3){
            containerAlert.setVisibility(View.VISIBLE);
            containerAlert.setBackgroundColor(Color.parseColor("#FFDFAE0C"));
            alertInfo.setText("Akun anda mendapatkan "+sharedPrefManager.getUser().getAlert()+" peringatan. Akun DIBEKUKAN sementara oleh sistem.");
        }

        if (sharedPrefManager.getUser().getAlert() >= 5){
            containerAlert.setVisibility(View.VISIBLE);
            containerAlert.setBackgroundColor(Color.RED);
            alertInfo.setText("Akun anda telah DIBLOKIR oleh Sistem.");
        }
    }

    public void getProfile(){
        baseApiService.getProfile(sharedPrefManager.getUser().getIdUser().toString())
                .enqueue(new Callback<GetProfile>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(@NonNull Call<GetProfile> call, @NonNull Response<GetProfile> response) {
                        if (response.isSuccessful()){
                            if (response.isSuccessful()){
                                sharedPrefManager.saveUser(response.body().getResults().getUser());

                                int jmlh = response.body().getResults().getLahan().getTotal();
                                int verif = response.body().getResults().getLahan().getTerverif();
                                int notVerif = response.body().getResults().getLahan().getNotverif();

                                txtLahan.setText(jmlh+" Total Iklan Lahan Anda.\n"+verif+" Iklan TERVERIFIKASI.\n"+notVerif+" Iklan Belum TERVERIFIKASI.");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetProfile> call, Throwable t) {

                    }
                });
    }

}
