package com.sewatanah.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sewatanah.API.BaseApiService;
import com.sewatanah.API.UtilsApi;
import com.sewatanah.Adapter.LahanAdapter.AdapterLahan;
import com.sewatanah.Adapter.LahanAdapter.AdapterLahan2;
import com.sewatanah.Models.GetLahan.GetLahan;
import com.sewatanah.Models.GetLahan.ResultsItem;
import com.sewatanah.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentDashboard extends Fragment {

    BaseApiService mApiInterface;
    RecyclerView mRecyclerView;
    RecyclerView mRecyclerView2;
    RecyclerView.Adapter mAdapter;
    RecyclerView.Adapter mAdapter2;

    public fragmentDashboard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashbord, container, false);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView2 = view.findViewById(R.id.recyclerView2);

        mApiInterface = UtilsApi.getAPIService();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView2.setLayoutManager(mLayoutManager2);

        Call<GetLahan> lahanCall = mApiInterface.getLahan();
        lahanCall.enqueue(new Callback<GetLahan>() {
            @Override
            public void onResponse(@NonNull Call<GetLahan> call, @NonNull Response<GetLahan> response) {
                List<ResultsItem> results = response.body().getResults();

                mAdapter = new AdapterLahan(results);
                mAdapter2 = new AdapterLahan2(results);
                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView2.setAdapter(mAdapter2);
            }

            @Override
            public void onFailure(Call<GetLahan> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });

        return view;
    }

}
