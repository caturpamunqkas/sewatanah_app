package com.sewatanah.API;

import com.sewatanah.Models.GetLahan.GetLahan;
import com.sewatanah.Models.GetProfile.GetProfile;
import com.sewatanah.Models.GetUser.GetUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by DESKTOP-TJTR on 1/31/2018.
 */

public interface BaseApiService {

    @Headers("api_key: sewatanah")
    @FormUrlEncoded
    @POST("user/login")
    Call<GetUser> loginRequest(@Field("username") String username,
                               @Field("password") String password);

    @Headers("api_key: sewatanah")
    @GET("dashboard")
    Call<GetLahan> getLahan();

    @Headers("api_key: sewatanah")
    @FormUrlEncoded
    @POST("profile/index")
    Call<GetProfile> getProfile(@Field("id_user") String id_user);
}
