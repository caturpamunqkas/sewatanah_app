package com.sewatanah.API;

/**
 * Created by DESKTOP-TJTR on 1/31/2018.
 */

public class UtilsApi {

    public static final String BASE_URL_API = "http://192.168.43.170/sewatanah_web/api/";

    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
